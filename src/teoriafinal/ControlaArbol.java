/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teoriafinal;

import javax.swing.JOptionPane;

/**
 *
 * @author Luis Carlos
 */
public class ControlaArbol {
    String texto;
    ControladorP pila= new ControladorP();
    public ControlaArbol(String texto) {
        this.texto=texto;
    }
    
    public boolean seleccionar(){
        boolean estado=false;
        switch(texto.charAt(0)){
            case 'V':
                //Replace <RV>
                pila.instruccion5();
                    switch(texto.charAt(1)){
                         case 'V':
                             //E3
                             JOptionPane.showMessageDialog(null, "Se esperaba una I o λ", texto, 0);
                            break;
                        case 'I':
                            pila.instruccion6();
                                    //RVI
                                    switch(texto.charAt(2)){
                                        case 'V':
                                            //E3
                                            JOptionPane.showMessageDialog(null, "Se esperaba una I o λ", texto, 0);
                                            break;
                                        case 'I':
                                            //Replace <RVII>
                                            pila.instruccion7();
                                            ///////////////////////
                                                switch(texto.charAt(2)){
                                                    case 'V':
                                                        //E3
                                                        JOptionPane.showMessageDialog(null, "Se esperaba una I o λ", texto, 0);
                                                        break;
                                                    case 'I':
                                                        //Desapile y avance
                                                        pila.instruccion3();
                                                            if(texto.charAt(3)=='λ'){
                                                                estado=true;
                                                            }
                                                        break;
                                                    case 'X':
                                                        //E3
                                                        JOptionPane.showMessageDialog(null, "Se esperaba una I o λ", texto, 0);
                                                        break;
                                                    case 'L':
                                                        //E3
                                                        JOptionPane.showMessageDialog(null, "Se esperaba una I o λ", texto, 0);
                                                        break;
                                                    case 'λ':
                                                        pila.instruccion4();
                                                        break;
                                                    default:
                                                        System.out.println("Se esperaba una L, X, V O I");
                                                        break;

                                                }
                                            ///////////////
                                            
                                            break;
                                        case 'X':
                                            //E3
                                            JOptionPane.showMessageDialog(null, "Se esperaba una I o λ", texto, 0);
                                            break;
                                        case 'L':
                                            //E3
                                            JOptionPane.showMessageDialog(null, "Se esperaba una I o λ", texto, 0);
                                            break;
                                        case 'λ':
                                            pila.instruccion4();
                                            break;
                                        default:
                                            System.out.println("Se esperaba una L, X, V O I");
                                            break;

                                    }
                            break;
                        case 'X':
                            //E3
                             JOptionPane.showMessageDialog(null, "Se esperaba una I o |-", texto, 0);
                            break;
                        case 'L':
                            //E3
                             JOptionPane.showMessageDialog(null, "Se esperaba una I o |-", texto, 0);
                            break;
                        case 'λ':
                             pila.instruccion3();
                            break;
                        default:
                            //E3
                            JOptionPane.showMessageDialog(null, "Se esperaba una I o |-", texto, 0);
                            break;
                    }
                break;
            case 'I':
                break;
            case 'X':
                break;
            case 'L':
                break;
            default:
                JOptionPane.showMessageDialog(null, "Se esperaba una L, X, V O I", texto, 0);
                break;
        }
        return estado;
    }
    
    
}
