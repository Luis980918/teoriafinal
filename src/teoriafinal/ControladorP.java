/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teoriafinal;

import java.util.Stack;

/**
 *
 * @author Luis Carlos
 */
public class ControladorP {
    Stack<String> pila=new Stack<String>();
    /*public void ejemplo(String d){
        pila.push(d);
        pila.push("L");
        System.out.println(pila.pop());
    }*/
//Inicializamos la pila con su configuración inicial, "Pila vacía y <R>"
    public ControladorP() {
        pila.push("<R>");
    }
    public String instruccion1(){
        pila.pop();
        return pila.push("<RI>");
    }
    public String instruccion2(){
        pila.pop();
        return pila.push("<RII>");
    }
    public void instruccion3(){
        pila.pop();
    }
    public void instruccion4(){
        pila.pop();
    }
    public String instruccion5(){
        pila.pop();
        return pila.push("<RV>");
    }
    public String instruccion6(){
        pila.pop();
        return pila.push("<RVI>");
    }
    public String instruccion7(){
        pila.pop();
        return pila.push("<RVII>");
    }
    public String instruccion8(){
        pila.pop();
        return pila.push("<RX>");
    }
    public String instruccion9(){
        pila.pop();
        return pila.push("<RXX>");
    }
    public String instruccion10(){
        pila.pop();
        return pila.push("<RXXX>");
    }
    public String instruccion11(){
        pila.pop();
        return pila.push("<RXL>");
    }
    public String instruccion12(){
        pila.pop();
        return pila.push("<RL>");
    }
    public String instruccion13(){
        pila.pop();
        return pila.push("<RLX>");
    }
    public String instruccion14(){
        pila.pop();
        return pila.push("<RLXX>");
    }
    public String instruccion15(){
        pila.pop();
        return pila.push("<RLXXX>");
    }
}
